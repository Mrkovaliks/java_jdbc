package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import Model.Cliente;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ClienteDAO {

    private DataSource datasource;

    public ClienteDAO(DataSource datasouce) {
        this.datasource = datasouce;
    }


    public ArrayList<Cliente> readAll() {
        try {
            String SQL = "SELECT * FROM clientes";
            PreparedStatement ps = datasource.getConnection().prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();

            ArrayList<Cliente> lista = new ArrayList<Cliente>();
            while (rs.next()) {
                Cliente cli = new Cliente();
                cli.setId(rs.getInt("id"));
                cli.setNome(rs.getString("Nome"));
                cli.setEmail(rs.getString("email"));
                cli.setTelefone(rs.getString("telefone"));
                lista.add(cli);
            }

            ps.close();
            return lista;
            //status ?
        } catch (SQLException ex) {
            System.err.println("Erro de recuperar " + ex.getMessage());
        } catch (Exception ex) {
            System.err.println("Erro geral " + ex.getMessage());
        }
        return null;
    }

    /**
     *
     * @param nome
     * @param email
     * @param telefone
     */
    public void inserir(String nome, String email, String telefone) {
        try {
            String comando = "insert into clientes (nome, email, telefone) values('" + nome + "','" + email + "','" + telefone + "')";
            PreparedStatement ps = datasource.getConnection().prepareStatement(comando);
            ps.executeUpdate();
            ps.close();
            JOptionPane.showMessageDialog(null, "inserção de cliente foi feita com sucesso.");
        } catch (SQLException er) {
            System.out.println("Erro ao inserir, tente novamente mais tarde." + er);

        }
    }

    public void remover(int id) {
        try {
            String comando = "Delete from clientes where id =" + id;
            PreparedStatement ps = datasource.getConnection().prepareStatement(comando);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Remoção do cliente foi feita com sucesso.");
        } catch (SQLException er) {
            System.out.println("Erro na remoção, Só é possivel fazer a remoção pelo o Id do cliente" + er);
        }
    }
}
